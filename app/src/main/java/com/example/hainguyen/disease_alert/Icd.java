package com.example.hainguyen.disease_alert;

import android.util.Log;

/**
 * Created by hainguyen on 2017-08-29.
 */

class Icd {
    private String _id;
    private int count;
    private IcdInfo icd_info;
    IcdInfo getIcd_info() {
        if (icd_info == null){
            return IcdInfo.getIcdInfo(_id);
        }
        return icd_info;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
