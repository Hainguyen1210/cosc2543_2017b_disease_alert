package com.example.hainguyen.disease_alert;

import android.Manifest;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    static boolean isInForegroundMode = true, isVerbose = true;
    boolean isOnViewMode = false, isLoading = false;
    GoogleMap mMap;
    LatLng displayLocation = null, currentLocation = null;
    final int
            LOCATION_REQUEST_CODE = 1,
            updateInterval = 50000,
            updateFastestInterval = 20000;
    double nearByRange = 1, nearbyRangeMax = 20;
    GeofencingClient client;
    static int totalCases;
    static Disease[] diseases;
    static ArrayList<String> icdNames = new ArrayList<>();
    ArrayList<Geofence> geofences = new ArrayList<>();
    int PLACE_PICKER_REQUEST;

    private enum bottomSheetState {
        BS_HIDDEN,
        BS_EXPANDED,
        BS_COLLAPSED
    }
    // UI variables
    View map_view;
    NestedScrollView bottomSheet;
    PendingIntent pendingIntent;
    ListView lv_diseases;
    TextView tv_area_name, tv_num_cases;
    ProgressBar loading;
    SeekBar sb_nearbyRange;
    Button bt_toggleViewMode, bt_closeBottomSheet;
    Button bt_send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(isVerbose) Log.d("pipi", "oncreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        map_view = findViewById(R.id.map);
        tv_area_name = findViewById(R.id.tv_area_name);
        tv_num_cases = findViewById(R.id.tv_num_cases);
        sb_nearbyRange = findViewById(R.id.seekBar_nearbyRange);
        lv_diseases = findViewById(R.id.lv_diseases);
        bottomSheet = findViewById(R.id.bottom_sheet);
        loading = findViewById(R.id.loading);
//        loading.setTop(map_view.getMeasuredHeight() - loading.getMeasuredHeight());
        bt_closeBottomSheet = findViewById(R.id.close);
        bt_closeBottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetSlide(bottomSheetState.BS_HIDDEN);
            }
        });
        bt_toggleViewMode = findViewById(R.id.bt_picker_mode);
        bt_toggleViewMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isOnViewMode = !isOnViewMode;
                if (!isOnViewMode){
                    Toast.makeText(MapsActivity.this, "Showing surrounding cases", Toast.LENGTH_SHORT).show();
                    displayData(currentLocation, nearByRange);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
                } else {
                    Toast.makeText(MapsActivity.this, "Touch a place to show data" , Toast.LENGTH_LONG).show();
                }
                bt_toggleViewMode.setBackgroundResource(isOnViewMode? R.mipmap.ic_eye : R.mipmap.ic_location);
            }
        });


//        final String appId = "com.facebook.orca";
        final String appId = "com.facebook.katana";

        bt_send = findViewById(R.id.bt_fb_send);
        bt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Sharing sharing = Sharing.getInstancce();
                if (!sharing.isAppinstalled(getApplicationContext(), appId)){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appId)));
                }
                sharing.sharePhoto(MapsActivity.this, mMap);
            }
        });

        sb_nearbyRange.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
//                if (isVerbose) Log.d("pipi", "seekbar "+Integer.toString(i));
                nearByRange = i+1;
                float zoom = (float) (13.0 - (float)nearByRange*(0.13157894736));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(displayLocation, zoom));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(MapsActivity.this, "Showing cases " + (int)nearByRange + " km surround", Toast.LENGTH_SHORT).show();
                displayData(displayLocation, nearByRange);
            }
        });

    }

    /*
    connect location service client
     */
    @Override
    protected void onStart() {
        if(isVerbose) Log.d("pipi", "onStart");
        super.onStart();
        client = LocationServices.getGeofencingClient(this);
    }

    /*
    check/ask for location permission
    listener: show/hide maker title
    listener: show/hide bottom sheet
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        if(isVerbose) Log.d("pipi", "onMapReady");
        mMap = googleMap;
        UiSettings uisettings = mMap.getUiSettings();
        uisettings.setZoomControlsEnabled(false);
        uisettings.setRotateGesturesEnabled(false);
        uisettings.setCompassEnabled(false);
        uisettings.setMyLocationButtonEnabled(false);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
            return;
        }
        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                if (diseases == null) return;
                for (Disease disease: diseases) {
                    if (disease.getMarker() == null) return;
                    disease.getMarker().setVisible(mMap.getCameraPosition().zoom >= 14);
                }
            }
        });
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                displayDiseaseInfo(marker, true);
            }
        });

        mMap.setOnMarkerClickListener(new OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                displayDiseaseInfo(marker, true);
                return false;
            }
        });

        mMap.setOnCircleClickListener(new GoogleMap.OnCircleClickListener() {
            @Override
            public void onCircleClick(Circle circle) {
                Marker marker = (Marker) circle.getTag();
                if (marker == null) return;

                if (getBottomSheetState() == bottomSheetState.BS_EXPANDED && bottomSheet.getTag() == marker.getTag()) {
                    bottomSheetSlide(bottomSheetState.BS_HIDDEN);
                    return;
                }
                displayDiseaseInfo(marker, true);
                mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
            }
        });

        // hide bottom sheet when user clicks on map
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                bottomSheetSlide(bottomSheetState.BS_HIDDEN);
                if (isOnViewMode) {
                    displayLocation = latLng;
                    displayData(displayLocation, nearByRange);
                }
            }
        });
        setupLocation();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(isVerbose) Log.d("pipi", "onRequestPermission");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_REQUEST_CODE) {
            setupLocation();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isInForegroundMode = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isInForegroundMode = true;
//        if (mRequestingLocationUpdates) {
////            startLocationUpdates();
//        }
    }

    private void displayDiseaseInfo(Marker marker, boolean isExpanded){
        if(isVerbose) Log.d("pipi", "displayDiseaseInfo");
        Disease disease = (Disease) marker.getTag();
        if (disease != null){
            tv_area_name.setText(disease.getWard() + ", " + disease.getDistrict() + ", " + disease.getProvince());
            tv_num_cases.setText(String.format("Total cases: %s", Integer.toString(disease.getTotal())));

            ArrayList<HashMap<String,String>> arrayList=new ArrayList<>();
            for (Icd icd : disease.getIcds()){
                if (isVerbose) Log.d("pipi", icd.get_id());
                IcdInfo icdInfo = icd.getIcd_info();
                if (icdInfo == null) continue;
                HashMap<String,String> hashMap=new HashMap<>();//create a hashmap to store the data in key value pair
                hashMap.put("name", icdInfo.getName() + " (cases: " + icd.getCount() + ")");
                hashMap.put("desc", icdInfo.getDesc());
                arrayList.add(hashMap);//add the hashmap into arrayList
            }
            String[] from={"name","desc"};//string array
            int[] to={R.id.tv_disease_name,R.id.tv_disease_desc};//int array of views id's
            SimpleAdapter simpleAdapter=new SimpleAdapter(this,arrayList,R.layout.disease_desc,from,to);//Create object and set the parameters for simpleAdapter
            lv_diseases.setAdapter(simpleAdapter);//sets the adapter for listView

            bottomSheetSlide(isExpanded? bottomSheetState.BS_EXPANDED : bottomSheetState.BS_COLLAPSED);
            bottomSheet.scrollTo(0,0);
            bottomSheet.setTag(disease);
        }
    }

    private void bottomSheetSlide(bottomSheetState state){
//        if(isVerbose) Log.d("pipi", "to state " + state.toString());
        bottomSheet.setVisibility(View.VISIBLE);

        float start, end = 0;
        start = bottomSheet.getTop();
        switch (state){
            case BS_EXPANDED:
                end = map_view.getBottom() - bottomSheet.getMeasuredHeight();
                break;
            case BS_COLLAPSED:
                end = map_view.getBottom() - bottomSheet.getMeasuredHeight()/2;
                break;
            case BS_HIDDEN:
                end = map_view.getBottom();
                break;
            default:
                Toast.makeText(this, "bottomsheet default", Toast.LENGTH_SHORT).show();
                break;
        }
//        if(isVerbose) Log.d("pipi", "start " + start + " end " + end + "(" + map_view.getBottom() + " " + bottomSheet.getMeasuredHeight() +")");
        bottomSheet.animate()
                .translationYBy(start)
                .translationY(end);
    }

    private void showDisplayArea(){
        if (isVerbose) Log.d("pipi", "showDisplayArea");

    }

    private bottomSheetState getBottomSheetState() {
        int[] location = new int[2];
        bottomSheet.getLocationOnScreen(location);

//        if(isVerbose) Log.d("pipi", Integer.toString(location[0]) + " " +
//                Integer.toString(location[1]) + " " +
//                Integer.toString(map_view.getTop()) + " " +
//                Integer.toString(map_view.getBottom()) );

        if (location[1] < map_view.getBottom()) {
            return bottomSheetState.BS_EXPANDED;
        } else if (location[1] == map_view.getBottom()){
            return bottomSheetState.BS_HIDDEN;
        } else return null;
    }

    private void checkGps(){
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("Location service is not enabled");
            dialog.setPositiveButton("Turn ON", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Toast.makeText(MapsActivity.this, "Cannot access current location", Toast.LENGTH_LONG).show();
                }
            });
            dialog.show();
        }
    }

    /**
     * try to access user location
     * move camera to current location, alert if user is in danger area
     * alert if unable to access location
     */
    private void setupLocation() {
        if(isVerbose) Log.d("pipi", "setupLocation");
        try {
            mMap.setMyLocationEnabled(true);
            FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location == null) {
                        checkGps();
                        return;
                    }
                    displayLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    currentLocation = displayLocation;
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(displayLocation, 15));
                    displayData(displayLocation, nearByRange);
                }
            });

            LocationRequest mLocationRequest = new LocationRequest()
                    .setInterval(updateInterval)
                    .setFastestInterval(updateFastestInterval)
                    .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                    new LocationCallback() {
                        @Override
                        public void onLocationResult(LocationResult locationResult) {
                            currentLocation = new LatLng(locationResult.getLastLocation().getLatitude(), locationResult.getLastLocation().getLongitude());
                            if (!isOnViewMode){
                                displayLocation = currentLocation;
                                displayData(displayLocation, nearByRange);
                            }
                        }
                    },
                    null /* Looper */);
        }
        catch (SecurityException ex){
            Toast.makeText(this, "Cannot access current location", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Fetch diseases near the current location and display them as danger area on the map
     * update geofence list
     */
    private void displayData(LatLng here, double radius){
        if(isVerbose) Log.d("pipi", "displayData");
        if (isLoading) return;
        isLoading = true;
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(here));
//        Toast.makeText(this, "Showing cases " + radius + " km around", Toast.LENGTH_SHORT).show();
        Fetcher fetcher = new Fetcher() {
            @Override
            protected void onFetched(String res) {
                if(isVerbose) Log.d("pipi", res);
                try {
                    JSONObject obj = new JSONObject(res);
                    JSONArray array = obj.getJSONArray("data");
                    diseases = new Gson().fromJson(array.toString(), Disease[].class);
                    if (diseases == null) return;
                    if(isVerbose) Log.d("pipi", "mark diseases");
                    totalCases = 0;
                    for (Disease disease: diseases){
                        for (Icd icd : disease.getIcds()) icd.getIcd_info();
                        totalCases += disease.getTotal();
                        if(isVerbose) Log.d("pipi", Integer.toString(disease.getTotal()));
                        markDisease(disease);
                    }
                    updateGeofences(client);        // UPDATE GOEFENCE LIST
//                    Toast.makeText(MapsActivity.this, "updated", Toast.LENGTH_SHORT).show();
                    loading.setVisibility(View.GONE);
                    isLoading = false;
                } catch (JSONException e) {
                    if(isVerbose) Log.d("pipi", "-----------------------------");
                    e.printStackTrace();
                }
            }
        };
//         bestlab.us:3333/diseases?lat=10.769760&lng=106.682709&radius=10
        loading.setVisibility(View.VISIBLE);
        String params = "lat=" + here.latitude + "&lng=" + here.longitude + "&radius=" + radius;
        if (isVerbose) Log.d("pipi", params);
        fetcher.execute("http://bestlab.us:3333/diseases", "GET", params);
    }

    /**
     * mark the location of a danger area, display info about the disease
     * Create geofence according to the disease location
     * @param disease
     */
    private void markDisease(final Disease disease){
        // more case = more opaque
        int alpha = (disease.getTotal() * 5) + 10;
        if (alpha > 80) alpha = 80;
        int
            markerStrokeColor = Color.WHITE,
            markerFillColor = Color.WHITE,
            markerBoundColor = Color.argb(0,0,0,0),
            circleStrokeColor = Color.argb(0, 0, 0, 0),
            circleFillColor = Color.argb(alpha, 255, 0, 0);

        double lat = (disease.getNeLat() - disease.getSwLat())/2 + disease.getSwLat();
        double lng = (disease.getNeLng() - disease.getSwLng())/2 + disease.getSwLng();

        // DRAW MAKER
        LatLng latLng = new LatLng(lat, lng);
        Marker marker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(
                    BitmapDescriptorFactory.fromBitmap(
                        createBitmap(Integer.toString(disease.getTotal()), markerBoundColor)
                    )
                )
                .visible(false)
        );
        //bind marker & disease
        marker.setTag(disease);
        disease.setMarker(marker);

        // DRAW DANGER AREA BY CIRCLE
            // calculate the distance between 2 cornjkers and use the larger one as radius
        float results1[] = new float[1];
        float results2[] = new float[1];
        Location.distanceBetween(lat, lng, disease.getNeLat(), disease.getNeLng(), results1);
        Location.distanceBetween(lat, lng, disease.getSwLat(), disease.getSwLng(), results2);
        double circleRadius = (results1[0] < results2[0]? results1[0]: results2[0]);
            // add circle
        mMap.addCircle(new CircleOptions()
                .center(latLng)
                .radius(circleRadius)
                .strokeColor(circleStrokeColor)
                .fillColor(circleFillColor)
                .clickable(true)
        ).setTag(marker);

        // ADD GEOFENCE
        addGeofenceInstance(latLng, (float) circleRadius, disease);
    }

    private Bitmap createBitmap(String text, int color){
        // paint defines the text color, stroke width and size
        Paint paint = new Paint();
        paint.setTextSize(40);
        paint.setFakeBoldText(true);
        paint.setColor(Color.BLACK);
        paint.setAntiAlias(true);
        paint.setShadowLayer(5, 0, 0, Color.WHITE);
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);

        Bitmap bmp = Bitmap.createBitmap((int) (bounds.width() * 1.5), (int) (bounds.height() * 1.5), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmp);

        // modify canvas
        canvas.drawColor(color);
        canvas.drawText(text, (float) (bounds.width() * 0.25), paint.getTextSize(), paint);
        return bmp;
    }

    private PendingIntent getPendingIntent(){
        if (pendingIntent != null){
            return pendingIntent;
        }
        Intent newIntent = new Intent(this, GeofenceTransitionsIntentService.class);
        pendingIntent = PendingIntent.getService(this, 0, newIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        return pendingIntent;
    }

    /**
     * update the geofence list
     * @param client
     */
    private void updateGeofences(GeofencingClient client){
        try{
            if (geofences.size() < 1) return;

            GeofencingRequest geofencingRequest = new GeofencingRequest.Builder()
                    .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_DWELL )
                    .addGeofences(geofences)
                    .build();

            client.removeGeofences(getPendingIntent());
            client.addGeofences(geofencingRequest, getPendingIntent());
        } catch (SecurityException e) {
            Toast.makeText(this, "cannot update geofence", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * add a geofence instance to the geofenceList
     * @param ll location of the geofence
     * @param radius the circular region of the geofence
     */
    private void addGeofenceInstance(LatLng ll, float radius, Disease disease){
        if (client == null) return;
        if (geofences == null) return;
        geofences.add(new Geofence.Builder()
                .setRequestId(disease.toString())
                .setCircularRegion(ll.latitude, ll.longitude, radius)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setLoiteringDelay(500)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_DWELL | Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build()
        );
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        }
    }
}
