package com.example.hainguyen.disease_alert;
/**
 * Created by s3562060 on 7/7/17.
 */

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by s3562060 on 6/30/17.
 */

public abstract class Fetcher extends AsyncTask<String,Void,String> {
    protected abstract void onFetched(String res);

    @Override
    protected void onPostExecute(String s) {
        onFetched(s);
    }

    @Override
    protected String doInBackground(String... strings) {
        return makeHttpRequest(strings[0], strings[1], strings[2]);
    }

    private String makeHttpRequest(String urlStr, String method, String paramsStr){
        StringBuilder response = new StringBuilder();
        try {
            URL url = new URL(urlStr + (method.equals("GET") ? ("?" + paramsStr) :""));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            if (method.equals("POST")){
                connection.setRequestMethod( "POST" );
                connection.setRequestProperty( "Content-Type", "application/json");
                DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
                JSONObject jsonObject = new JSONObject();
                String[] params = paramsStr.split("&");
                for (String param : params){
                    String[] parts = param.split("=");
                    jsonObject.put(parts[0],parts[1]);
                }
                dataOutputStream.writeBytes(jsonObject.toString());
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line = "";

            do {
                response.append(line);
                line = reader.readLine();
            } while (line != null);
            connection.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response.toString();
    }
}
