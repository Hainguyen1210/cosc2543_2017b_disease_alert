package com.example.hainguyen.disease_alert;

import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hainguyen on 2017-08-01.
 */

class Disease {
    private String _id;
    private double neLat, neLng, swLat, swLng, diffLatLngSquared;
    private int total;
    private String ward, district, province;
    private List<Icd> icds;
    private Marker marker;

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public String get_id() {
        return _id;
    }

    public double getNeLat() {
        return neLat;
    }

    public double getNeLng() {
        return neLng;
    }

    public double getSwLat() {
        return swLat;
    }

    public double getSwLng() {
        return swLng;
    }

    public String getWard() {
        return ward;
    }

    public String getDistrict() {
        return district;
    }

    public String getProvince() {
        return province;
    }

    public List<Icd> getIcds() {
        return icds;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}