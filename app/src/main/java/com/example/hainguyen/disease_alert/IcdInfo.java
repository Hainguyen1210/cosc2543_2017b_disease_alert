package com.example.hainguyen.disease_alert;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by hainguyen on 7/20/2017.
 */

class IcdInfo {
    private static ArrayList<IcdInfo> icd_infos = new ArrayList<>();
    private static ArrayList<String> fetchingIcd_idList = new ArrayList<>();
    private String _id, code, name, desc;

    /*
    find icd info in the local list, if not available, fetch online
     */
    static IcdInfo getIcdInfo(String _id){
        for (IcdInfo icd_info: icd_infos){
            if (Objects.equals(icd_info.get_id(), _id)){
                return icd_info;
            }
        }
        // fetch if it is not fetching
        for (String fetchingIcd_id : fetchingIcd_idList){
            if (fetchingIcd_id.equals(_id)) return null;
        }
        fetchingIcd_idList.add(_id);
        getNewIcdInfo(_id);
        return null;
    }

    /*
    Fetch new icd info, if successful, add to the local array
     */
    private static void getNewIcdInfo(final String _id){

        Fetcher fetcher = new Fetcher() {
            @Override
            protected void onFetched(String res) {
                try {
                    JSONObject obj = new JSONObject(res);
                    JSONObject jsonIcdInfo = obj.getJSONObject("data");
                    if (MapsActivity.isVerbose) Log.d("pipi", jsonIcdInfo.toString());
                    IcdInfo icdInfo= new Gson().fromJson(String.valueOf(jsonIcdInfo), IcdInfo.class);
                    icd_infos.add(icdInfo);
                    fetchingIcd_idList.remove(_id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        fetcher.execute("http://bestlab.us:3333/icds/" + _id, "GET", "");

    }

    private int severity;

    public int getSeverity() {
        return severity;
    }

    public String get_id() {
        return _id;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

}
