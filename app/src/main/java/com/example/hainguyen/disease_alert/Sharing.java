package com.example.hainguyen.disease_alert;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.view.View;

import com.facebook.FacebookDialog;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.MessageDialog;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.maps.GoogleMap;

/**
 * Created by hainguyen on 2017-08-31.
 */

class Sharing {
    private static Sharing sharing = null;
    private Sharing() {

    }

    static Sharing getInstancce(){
        if (sharing == null){
            sharing = new Sharing();
        }
        return sharing;
    }

    private Bitmap takePhoto(View view){
        View rootView = view.getRootView();
        rootView.setDrawingCacheEnabled(true);
        rootView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        Bitmap bitmap = Bitmap.createBitmap(rootView.getDrawingCache());
        rootView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    void sharePhoto(final Activity activity, GoogleMap map){
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                SharePhoto photo = new SharePhoto.Builder()
                        .setBitmap(snapshot)
                        .build();
                SharePhotoContent sharePhotoContent = new SharePhotoContent.Builder()
                        .addPhoto(photo)
                        .build();
                ShareDialog.show(activity, sharePhotoContent);
//                MessageDialog.show(activity, sharePhotoContent);
            }
        };
        map.snapshot(callback);
    }

    boolean isAppinstalled(Context context, String appName){
        try{
            context.getPackageManager().getApplicationInfo(appName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

}
